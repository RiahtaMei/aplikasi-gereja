import React, { Component } from "react";
import {
    View,
    ScrollView,
    FlatList,
    Animated,
    TouchableOpacity,
    Linking,
    Alert,
    TextInput
} from "react-native";
import { BaseStyle, BaseColor, Images } from "@config";
import {
    Header,
    SafeAreaView,
    Icon,
    Text,
    StarRating,
    Tag,
    Image,
    PlaceItem,
    CardList,
    Button
} from "@components";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import * as Utils from "@utils";
import styles from "./styles";

// Load sample data
import { PlaceListData, ReviewData } from "@data";

export default class DetailLayananGereja3 extends Component {
    constructor(props) {
        super(props);

        // Temp data define
        this.state = {
            name: "",
            email: "",
            address: "",
            phonenumber: "",
            password: "",
            newpassword: "",
            loading: false,
            success: {
                name: true,
                email: true,
                phonenumber: true,
                password: true,
                newpassword: true
            },
            collapseHour: true,
            index: 0,
            routes: [
                { key: "information", title: "Information" },
                { key: "review", title: "Review" },
                { key: "feedback", title: "Feedback" },
                { key: "map", title: "Map" }
            ],
            heightHeader: Utils.heightHeader(),
            information: [
                {
                    id: "1",
                    icon: "map-marker-alt",
                    title: "Address",
                    type: "map",
                    information: "667 Wiegand Gardens Suite, United States"
                },
                {
                    id: "2",
                    icon: "mobile-alt",
                    title: "Tel",
                    type: "phone",
                    information: "+903 9802-7892"
                },
                {
                    id: "3",
                    icon: "envelope",
                    title: "Email",
                    type: "email",
                    information: "liststar@passionui.com"
                },
                {
                    id: "4",
                    icon: "globe",
                    title: "Website",
                    type: "web",
                    information: "http://passionui.com"
                }
            ],
            workHours: [
                { id: "1", date: "Monday", hour: "09:0 AM - 18:00 PM" },
                { id: "2", date: "Tuesday", hour: "09:0 AM - 18:00 PM" },
                { id: "3", date: "Wednesday", hour: "09:0 AM - 18:00 PM" },
                { id: "4", date: "Thursday", hour: "09:0 AM - 18:00 PM" },
                { id: "5", date: "Friday", hour: "09:0 AM - 18:00 PM" },
                { id: "6", date: "Saturday", hour: "Close" },
                { id: "7", date: "Sunday", hour: "Close" }
            ],
            list: PlaceListData,
            relate: PlaceListData.slice(5, 7),
            facilities: [
                { id: "1", icon: "wifi", name: "Free Wifi", checked: true },
                { id: "2", icon: "bath", name: "Shower" },
                { id: "3", icon: "paw", name: "Pet Allowed" },
                { id: "4", icon: "bus", name: "Shuttle Bus" },
                { id: "5", icon: "cart-plus", name: "Supper Market" },
                { id: "6", icon: "clock", name: "Open 24/7" }
            ],
            region: {
                latitude: 1.352083,
                longitude: 103.819839,
                latitudeDelta: 0.009,
                longitudeDelta: 0.004
            }
        };
        this._deltaY = new Animated.Value(0);
    }

    onOpen(item) {
        Alert.alert(
            "Listar",
            "Do you want to open " + item.title + " ?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "OK",
                    onPress: () => {
                        switch (item.type) {
                            case "web":
                                Linking.openURL(item.information);
                                break;
                            case "phone":
                                Linking.openURL("tel://" + item.information);
                                break;
                            case "email":
                                Linking.openURL("mailto:" + item.information);
                                break;
                            case "map":
                                Linking.openURL(
                                    "http://maps.apple.com/?ll=37.484847,-122.148386"
                                );
                                break;
                        }
                    }
                }
            ],
            { cancelable: true }
        );
    }

    onCollapse() {
        Utils.enableExperimental();
        this.setState({
            collapseHour: !this.state.collapseHour
        });
    }

    render() {
        const { navigation } = this.props;
        const {
            heightHeader,
            information,
            workHours,
            collapseHour,
            list,
            relate,
            facilities,
            region
        } = this.state;
        const heightImageBanner = Utils.scaleWithPixel(250, 1);
        let { loading, name, email, address, phonenumber, password, newpassword, success } = this.state;
        return (
            <View style={{ flex: 1 }}>
                <Animated.View
                    style={[
                        styles.imgBanner,
                        {
                            height: this._deltaY.interpolate({
                                inputRange: [
                                    0,
                                    Utils.scaleWithPixel(140),
                                    Utils.scaleWithPixel(140)
                                ],
                                outputRange: [
                                    heightImageBanner,
                                    heightHeader,
                                    heightHeader
                                ]
                            })
                        }
                    ]}
                >
                    <Image source={Images.pelayanan2} style={{ flex: 1 }} />
                    <Animated.View
                        style={{
                            position: "absolute",
                            bottom: 15,
                            left: 20,
                            flexDirection: "row",
                            opacity: this._deltaY.interpolate({
                                inputRange: [
                                    0,
                                    Utils.scaleWithPixel(140),
                                    Utils.scaleWithPixel(140)
                                ],
                                outputRange: [1, 0, 0]
                            })
                        }}
                    >
                    </Animated.View>
                </Animated.View>
                <SafeAreaView
                    style={BaseStyle.safeAreaView}
                    forceInset={{ top: "always" }}
                >
                    {/* Header */}
                    <Header
                        title=""
                        renderLeft={() => {
                            return (
                                <Icon
                                    name="arrow-left"
                                    size={20}
                                    color={BaseColor.whiteColor}
                                />
                            );
                        }}
                        onPressLeft={() => {
                            navigation.goBack();
                        }}
                    />
                    <ScrollView
                        onScroll={Animated.event([
                            {
                                nativeEvent: {
                                    contentOffset: { y: this._deltaY }
                                }
                            }
                        ])}
                        onContentSizeChange={() => {
                            this.setState({
                                heightHeader: Utils.heightHeader()
                            });
                        }}
                        scrollEventThrottle={8}
                    >
                        <View style={{ height: 255 - heightHeader }} />
                        <View
                            style={{
                                paddingHorizontal: 20,
                                marginBottom: 20
                            }}
                        >
                            <View style={styles.lineSpace}>
                                <Text title3 semibold
                                semibold
                                style={{
                                    textAlign: 'center' 
                                }}
                                >
                                    Pelayanan Doa Kesembuhan
                                </Text>
                            </View>
                        </View>
                        <View style={styles.contentDescription}>
                            <Text body2 style={{ lineHeight: 20 }}>
                            Shalom teman-teman.
                            Bagi teman-teman yang ingin melaksanakan Pelayanan Doa Kesembuhan,
                            boleh mendaftarkan dirinya pada layanan 
                            "Pelayanan Doa Kesembuhan yang akan dilaksanakan
                            pada waktu dan tempat yang ditulis dibawah ini.
                            </Text>
                            <View
                                style={{
                                    paddingVertical: 20,
                                    flexDirection: "row",
                                    borderBottomWidth: 1,
                                    borderColor: BaseColor.textSecondaryColor
                                }}
                            >
                                <View style={{ flex: 1 }}>
                                    <Text caption1 grayColor>
                                        Date 
                                    </Text>
                                    <Text headline style={{ marginTop: 5 }}>
                                        Jan 26, 2020
                                    </Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                    <Text caption1 grayColor>
                                        Location
                                    </Text>
                                    <Text headline style={{ marginTop: 5 }}>
                                        GKI Kelapa Gading
                                    </Text>
                                </View>
                            </View>


                            {/* <View
                                style={{
                                    height: 180,
                                    paddingVertical: 20
                                }}
                            >
                                <MapView
                                    provider={PROVIDER_GOOGLE}
                                    style={styles.map}
                                    region={region}
                                    onRegionChange={() => {}}
                                >
                                    <Marker
                                        coordinate={{
                                            latitude: 1.352083,
                                            longitude: 103.819839
                                        }}
                                    />
                                </MapView>
                            </View> */}

                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text => this.setState({ phonenumber: text })}
                            autoCorrect={false}
                            placeholder="Full Name"
                            placeholderTextColor={
                                success.phonenumber
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={phonenumber}
                        />
                        
                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text => this.setState({ phonenumber: text })}
                            autoCorrect={false}
                            placeholder="Email"
                            placeholderTextColor={
                                success.phonenumber
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={phonenumber}
                        />

                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text => this.setState({ phonenumber: text })}
                            autoCorrect={false}
                            placeholder="Born Day"
                            placeholderTextColor={
                                success.phonenumber
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={phonenumber}
                        />

                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text => this.setState({ phonenumber: text })}
                            autoCorrect={false}
                            placeholder="Country"
                            placeholderTextColor={
                                success.phonenumber
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={phonenumber}
                        />

                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text => this.setState({ phonenumber: text })}
                            autoCorrect={false}
                            placeholder="Full Name"
                            placeholderTextColor={
                                success.phonenumber
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={phonenumber}
                        />

                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text => this.setState({ phonenumber: text })}
                            autoCorrect={false}
                            placeholder="State"
                            placeholderTextColor={
                                success.phonenumber
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={phonenumber}
                        />

                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text => this.setState({ phonenumber: text })}
                            autoCorrect={false}
                            placeholder="City"
                            placeholderTextColor={
                                success.phonenumber
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={phonenumber}
                        />

                            <Button
                                full
                                style={{ marginTop: 10 }}
                                onPress={() => navigation.navigate("KonfirKedatangan")}>
                                Daftar
                            </Button>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    }


}


