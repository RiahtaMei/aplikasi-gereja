import React, { Component } from "react";
import {
    View,
    ScrollView,
    FlatList,
    Animated,
    TouchableOpacity,
    Linking,
    Alert
} from "react-native";
import { BaseStyle, BaseColor, Images } from "@config";
import {
    Header,
    SafeAreaView,
    Icon,
    Text,
    StarRating,
    Tag,
    Image,
    PlaceItem,
    CardList,
    Button
} from "@components";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import * as Utils from "@utils";
import styles from "./styles";

// Load sample data
import { PlaceListData, ReviewData } from "@data";

export default class DetailRenunganHarian2 extends Component {
    constructor(props) {
        super(props);

        // Temp data define
        this.state = {
            collapseHour: true,
            index: 0,
            routes: [
                { key: "information", title: "Information" },
                { key: "review", title: "Review" },
                { key: "feedback", title: "Feedback" },
                { key: "map", title: "Map" }
            ],
            heightHeader: Utils.heightHeader(),
            information: [
                {
                    id: "1",
                    icon: "map-marker-alt",
                    title: "Address",
                    type: "map",
                    information: "667 Wiegand Gardens Suite, United States"
                },
                {
                    id: "2",
                    icon: "mobile-alt",
                    title: "Tel",
                    type: "phone",
                    information: "+903 9802-7892"
                },
                {
                    id: "3",
                    icon: "envelope",
                    title: "Email",
                    type: "email",
                    information: "liststar@passionui.com"
                },
                {
                    id: "4",
                    icon: "globe",
                    title: "Website",
                    type: "web",
                    information: "http://passionui.com"
                }
            ],
            workHours: [
                { id: "1", date: "Monday", hour: "09:0 AM - 18:00 PM" },
                { id: "2", date: "Tuesday", hour: "09:0 AM - 18:00 PM" },
                { id: "3", date: "Wednesday", hour: "09:0 AM - 18:00 PM" },
                { id: "4", date: "Thursday", hour: "09:0 AM - 18:00 PM" },
                { id: "5", date: "Friday", hour: "09:0 AM - 18:00 PM" },
                { id: "6", date: "Saturday", hour: "Close" },
                { id: "7", date: "Sunday", hour: "Close" }
            ],
            list: PlaceListData,
            relate: PlaceListData.slice(5, 7),
            facilities: [
                { id: "1", icon: "wifi", name: "Free Wifi", checked: true },
                { id: "2", icon: "bath", name: "Shower" },
                { id: "3", icon: "paw", name: "Pet Allowed" },
                { id: "4", icon: "bus", name: "Shuttle Bus" },
                { id: "5", icon: "cart-plus", name: "Supper Market" },
                { id: "6", icon: "clock", name: "Open 24/7" }
            ],
            region: {
                latitude: 1.352083,
                longitude: 103.819839,
                latitudeDelta: 0.009,
                longitudeDelta: 0.004
            }
        };
        this._deltaY = new Animated.Value(0);
    }

    onOpen(item) {
        Alert.alert(
            "Listar",
            "Do you want to open " + item.title + " ?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "OK",
                    onPress: () => {
                        switch (item.type) {
                            case "web":
                                Linking.openURL(item.information);
                                break;
                            case "phone":
                                Linking.openURL("tel://" + item.information);
                                break;
                            case "email":
                                Linking.openURL("mailto:" + item.information);
                                break;
                            case "map":
                                Linking.openURL(
                                    "http://maps.apple.com/?ll=37.484847,-122.148386"
                                );
                                break;
                        }
                    }
                }
            ],
            { cancelable: true }
        );
    }

    onCollapse() {
        Utils.enableExperimental();
        this.setState({
            collapseHour: !this.state.collapseHour
        });
    }

    render() {
        const { navigation } = this.props;
        const {
            heightHeader,
            information,
            workHours,
            collapseHour,
            list,
            relate,
            facilities,
            region
        } = this.state;
        const heightImageBanner = Utils.scaleWithPixel(250, 1);
        return (
            <View style={{ flex: 1 }}>
                <Animated.View
                    style={[
                        styles.imgBanner,
                        {
                            height: this._deltaY.interpolate({
                                inputRange: [
                                    0,
                                    Utils.scaleWithPixel(140),
                                    Utils.scaleWithPixel(140)
                                ],
                                outputRange: [
                                    heightImageBanner,
                                    heightHeader,
                                    heightHeader
                                ]
                            })
                        }
                    ]}
                >
                    <Image source={Images.renungan2} style={{ flex: 1 }} />
                    <Animated.View
                        style={{
                            position: "absolute",
                            bottom: 15,
                            left: 20,
                            flexDirection: "row",
                            opacity: this._deltaY.interpolate({
                                inputRange: [
                                    0,
                                    Utils.scaleWithPixel(140),
                                    Utils.scaleWithPixel(140)
                                ],
                                outputRange: [1, 0, 0]
                            })
                        }}
                    >
                    </Animated.View>
                </Animated.View>
                <SafeAreaView
                    style={BaseStyle.safeAreaView}
                    forceInset={{ top: "always" }}
                >
                    {/* Header */}
                    <Header
                        title=""
                        renderLeft={() => {
                            return (
                                <Icon
                                    name="arrow-left"
                                    size={20}
                                    color={BaseColor.whiteColor}
                                />
                            );
                        }}
                        onPressLeft={() => {
                            navigation.goBack();
                        }}
                    />
                    <ScrollView
                        onScroll={Animated.event([
                            {
                                nativeEvent: {
                                    contentOffset: { y: this._deltaY }
                                }
                            }
                        ])}
                        onContentSizeChange={() => {
                            this.setState({
                                heightHeader: Utils.heightHeader()
                            });
                        }}
                        scrollEventThrottle={8}
                    >
                        <View style={{ height: 255 - heightHeader }} />
                        <View
                            style={{
                                paddingHorizontal: 20,
                                marginBottom: 20
                            }}
                        >
                            <View style={styles.lineSpace}>
                                <Text title3 semibold
                                semibold
                                style={{
                                    textAlign: 'center' 
                                }}
                                >
                                    Bangku Persahabatan
                                </Text>
                            </View>
                        </View>
                        <View style={styles.contentDescription}>
                            <Text body2 style={{ lineHeight: 20 }}>
                            Shalom teman-teman.
                            Ibadah GLC Artha Mampang kembali lagi nih.
                            Ibadah GLC untuk kali ini dikhususkan untuk teman-teman di wijk Jakarta Selatan,
                            tapi kalau teman-teman yang lain mau datang boleh banget dong!
                            Tema ibadah kali ini adalah Yesus dan Indonesia.
                            Dengan Pembicara Mr. Ricky Tampubolon. 
                            Jangan lupa datang yahhhhh!
                            </Text>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    }


}


