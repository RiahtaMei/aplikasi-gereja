import React, { Component } from "react";
import { View, ScrollView, TextInput, AsyncStorage } from "react-native";
import { BaseStyle, BaseColor } from "@config";
import { Header, SafeAreaView, Icon, Button } from "@components";
import styles from "./styles";

export default class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            address: "",
            phonenumber: "",
            password: "",
            newpassword: "",
            username: "",
            loading: false,
            success: {
                name: true,
                username: true,
                email: true,
                address: true,
                phonenumber: true,
                password: true,
                newpassword: true
            }
        };
    }

    onSignUp() 
    {
        const { navigation } = this.props;
        let { name, email, address, phonenumber, password, newpassword, username, success } = this.state;

        if (name == "" || email == "" || address == "" || phonenumber == "" || password == "" || newpassword == "") {
            this.setState({
                success: {
                    ...success,
                    name: name != "" ? true : false,
                    email: email != "" ? true : false,
                    address: address != "" ? true : false,
                    phonenumber: phonenumber != "" ? true : false,
                    password: password != "" ? true : false,
                    newpassword: newpassword != "" ? true : false,
                    username: username != "" ? true : false
                }
            });
        } 
        else {
            this.setState(
                {
                    loading: true
                },
                () => {
                    setTimeout(() => {
                        this.setState({
                            loading: false
                        });
                        navigation.navigate("SignIn");
                    }, 500);
                }
            );
        }


    }


    getDataUsingPost()
    {
      const { navigation } = this.props;
      let { name, email, address, phonenumber, password, newpassword, username, success } = this.state;

      if (name == "" || email == "" || address == "" || phonenumber == "" || password == "" || newpassword == "" || username == "") {
          this.setState({
              success: {
                  ...success,
                  name: name != "" ? true : false,
                  email: email != "" ? true : false,
                  address: address != "" ? true : false,
                  phonenumber: phonenumber != "" ? true : false,
                  password: password != "" ? true : false,
                  newpassword: newpassword != "" ? true : false,
                  username: username != "" ? true : false
              }
          });
      } 

      else
      {
        this.setState(
          {
            loading: true
          },
          () => {
            setTimeout(() => {
                this.setState({
                    loading: false
                });

                var dataToSend = {
                  name: this.state.name, 
                  username: this.state.username,
                  adress: this.state.address,  
                  email: this.state.email,
                  phoneNumber: this.state.phonenumber,
                  password: this.state.password, 
                  newpassword: this.state.newpassword,
                };
                console.log(dataToSend);
                var formBody = [];
                for (var key in dataToSend) {
                  var encodedKey = encodeURIComponent(key);
                  var encodedValue = encodeURIComponent(dataToSend[key]);
                  formBody.push(encodedKey + "=" + encodedValue);
                }
                formBody = formBody.join("&");
                fetch('http://gcccapi.boksman.co.id/Auth/register', {
                  method: "POST", 
                  body: formBody, 
                  headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
                  },
                })
                .then((response) => response.json())
                .then((responseJson) => { 
                  console.log(responseJson);
                  if(responseJson.data[0].status == true)
                  {
                    let token = this.storeToken( responseJson.data );
          
                    // let data = this.storeToken([
                    //   responseJson.data[0].profile.id, 
                    //   responseJson.data[0].profile.name, 
                    //   responseJson.data[0].profile.email,
                    //   responseJson.data[0].profile.address,
                    //   responseJson.data[0].profile.username
                    // ]);
                    // this.props.navigation.navigate('Home',{ data, Alamat_Email: this.state.id });
                  }else{
                    alert(this.state.message);
                  }
                })
                .catch((error) => {
                  alert(JSON.stringify(error));
                  console.error(error);
                });
                navigation.navigate("SignIn");
            }, 500);
        }

        );
      }
    }
  
  
  
      componentDidMount() {
        this.getToken();
     }
    
    
     async storeToken(user) {
      try {
         var jsonOfItem = await AsyncStorage.setItem("userData", JSON.stringify(user));
         return jsonOfItem;
      } catch (error) {
        console.log("Something went wrong", error);
      }
    }
    async getToken(user) {
      try {
        let userData = await AsyncStorage.getItem("userData");
        let data = JSON.parse(userData);
        console.log(data);
      } catch (error) {
        console.log("Something went wrong", error);
      }
    }
    async deleteToken(user){
      try{
        let deleteData = await AsyncStorage.clear();
        await AsyncStorage.setItem("userData", JSON.stringify(deleteData));
      } catch (error) {
        console.log("Something went wrong", error);
      }
    }

































    render() {
        const { navigation } = this.props;
        let { loading, name, email, address, phonenumber, password, newpassword, username, success } = this.state;
        return (
            <SafeAreaView
                style={BaseStyle.safeAreaView}
                forceInset={{ top: "always" }}
            >
                <Header
                    title="Sign Up"
                    renderLeft={() => {
                        return (
                            <Icon
                                name="arrow-left"
                                size={20}
                                color={BaseColor.primaryColor}
                            />
                        );
                    }}
                    onPressLeft={() => {
                        navigation.goBack();
                    }}
                />
                <ScrollView>
                    <View style={styles.contain}>
                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 65 }]}
                            onChangeText={text => this.setState({ name: text })}
                            autoCorrect={false}
                            placeholder="Name"
                            placeholderTextColor={
                                success.name
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={name}
                        />
                        
                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text => this.setState({ username: text })}
                            autoCorrect={false}
                            placeholder="Username"
                            placeholderTextColor={
                                success.username
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={username}
                        />

                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text => this.setState({ phonenumber: text })}
                            autoCorrect={false}
                            placeholder="Phone Number"
                            placeholderTextColor={
                                success.phonenumber
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={phonenumber}
                        />
                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text => this.setState({ address: text })}
                            autoCorrect={false}
                            placeholder="Address"
                            placeholderTextColor={
                                success.address
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={address}
                        />
                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text =>
                                this.setState({ email: text })
                            }
                            autoCorrect={false}
                            placeholder="Email"
                            keyboardType="email-address"
                            placeholderTextColor={
                                success.email
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={email}
                        />
                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text =>
                                this.setState({ password: text })
                            }
                            autoCorrect={false}
                            placeholder="Password"
                            placeholderTextColor={
                                success.password
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={password}
                        />
                        <TextInput
                            style={[BaseStyle.textInput, { marginTop: 10 }]}
                            onChangeText={text =>
                                this.setState({ newpassword: text })
                            }
                            autoCorrect={false}
                            placeholder="Re-Password"
                            placeholderTextColor={
                                success.newpassword
                                    ? BaseColor.grayColor
                                    : BaseColor.primaryColor
                            }
                            value={newpassword}
                        />

                        <View style={{ paddingTop: "70%" }}></View>
                        <View style={{ width: "100%" }}>
                            <Button
                                full
                                style={{ marginTop: 20 }}
                                loading={this.state.loading}
                                onPress={() => this.getDataUsingPost()}
                                // onPress={() => navigation.navigate("KonfirOTP")}
                                // onPress={() => navigation.navigate("SignIn")}
                            >
                                Sign Up
                            </Button>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}
