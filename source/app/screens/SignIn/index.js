import React, { Component } from "react";
import { connect } from "react-redux";
import { AuthActions } from "@actions";
import { bindActionCreators } from "redux";
import { View, ScrollView, TouchableOpacity, TextInput, AsyncStorage } from "react-native";
import { BaseStyle, BaseColor } from "@config";
import { Header, SafeAreaView, Icon, Text, Button } from "@components";
import styles from "./styles";

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "test",
      password: "123456",
      loading: false,
      success: {
        id: true,
        password: true,
        message: 'Email atau Password Kamu salah'
      }
    };
  }

  onLogin() {
    const { id, password, success } = this.state;
    const { navigation } = this.props;
    if (id == "" || password == "") {
      this.setState({
        success: {
          ...success,
          id: false,
          password: false
        }
      });
    } else {

      this.setState(
        {
          loading: true
        },
        () => {
          this.props.actions.authentication(true, response => {
            if (response.success && id == "test" && password == "123456") 
            {
              navigation.navigate("Profile");
            } 
            else {
              this.setState({
                loading: false
              });
            }
          });
        }
      );

    }
  }


  getDataUsingPost()
  {
    const { id, password, success } = this.state;
    const { navigation } = this.props;

    if (id == "" || password == "") 
    {
      this.setState({
        success: {
          ...success,
          id: false,
          password: false
        }
      });

      console.log( "silahkan isi email dan password" );
    }
    else
    {
      this.setState(
        {
          loading: true
        },
        () => {
          this.props.actions.authentication(true, response => 
          {
            if (response.success) 
            {
              var dataToSend = {email: this.state.id, password: this.state.password};
              // console.log(dataToSend);
              var formBody = [];
              for (var key in dataToSend) {
                var encodedKey = encodeURIComponent(key);
                var encodedValue = encodeURIComponent(dataToSend[key]);
                formBody.push(encodedKey + "=" + encodedValue);
              }
              formBody = formBody.join("&");
              fetch('http://gcccapi.boksman.co.id/Auth/login', {
                method: "POST", 
                body: formBody, 
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
                },
              })
              .then((response) => response.json())
              .then((responseJson) => { 
                console.log(responseJson);
                if(responseJson.data[0].status == true)
                {
                  let token = this.storeToken( responseJson.data );
        
                  let data = this.storeToken([
                    responseJson.data[0].profile.id, 
                    responseJson.data[0].profile.name, 
                    responseJson.data[0].profile.email,
                    responseJson.data[0].profile.address,
                    responseJson.data[0].profile.username
                  ]);
                  this.props.navigation.navigate('Home',{ data, Alamat_Email: this.state.id });
                }else{
                  alert(this.state.message);
                }
              })
              .catch((error) => {
                alert(JSON.stringify(error));
                console.error(error);
              });
            } 
            else 
            {
              this.setState({
                loading: false
              });
            }
          }
          );
        }
      );
    }
  }



    componentDidMount() {
      this.getToken();
   }
  
  
   async storeToken(user) {
    try {
       var jsonOfItem = await AsyncStorage.setItem("userData", JSON.stringify(user));
       return jsonOfItem;
    } catch (error) {
      console.log("Something went wrong", error);
    }
  }
  async getToken(user) {
    try {
      let userData = await AsyncStorage.getItem("userData");
      let data = JSON.parse(userData);
      console.log(data);
    } catch (error) {
      console.log("Something went wrong", error);
    }
  }
  async deleteToken(user){
    try{
      let deleteData = await AsyncStorage.clear();
      await AsyncStorage.setItem("userData", JSON.stringify(deleteData));
    } catch (error) {
      console.log("Something went wrong", error);
    }
  }

    


  render() {
    const { navigation } = this.props;
    return (
      <SafeAreaView
        style={BaseStyle.safeAreaView}
        forceInset={{ top: "always" }}
      >
        <Header
          title="Sign In"
          renderLeft={() => {
            return (
              <Icon
                name="arrow-left"
                size={20}
                color={BaseColor.primaryColor}
              />
            );
          }}
          onPressLeft={() => {
            navigation.goBack();
          }}
        />
      

        <ScrollView>
          <View style={styles.contain}>
            <TextInput
              style={[BaseStyle.textInput, { marginTop: 65 }]}
              onChangeText={text => this.setState({ id: text })}
              onFocus={() => {
                this.setState({
                  success: {
                    ...this.state.success,
                    id: true
                  }
                });
              }}
              autoCorrect={false}
              placeholder="ID"
              placeholderTextColor={
                this.state.success.id
                  ? BaseColor.grayColor
                  : BaseColor.primaryColor
              }
              value={this.state.id}
              selectionColor={BaseColor.primaryColor}
            />
            <TextInput
              style={[BaseStyle.textInput, { marginTop: 10 }]}
              onChangeText={text => this.setState({ password: text })}
              onFocus={() => {
                this.setState({
                  success: {
                    ...this.state.success,
                    password: true
                  }
                });
              }}
              autoCorrect={false}
              placeholder="Password"
              secureTextEntry={true}
              placeholderTextColor={
                this.state.success.password
                  ? BaseColor.grayColor
                  : BaseColor.primaryColor
              }
              value={this.state.password}
              selectionColor={BaseColor.primaryColor}
            />
            <View style={{ paddingTop: "85%" }}></View>
            <View style={{ width: "100%" }}>  
              <Button
                full
                loading={this.state.loading}
                style={{ marginTop: 20 }}
                onPress={() => {this.getDataUsingPost();}}>
                Sign In
              </Button>

              <Button
                full
                style={{ marginTop: 10 }}
                onPress={() => navigation.navigate("SignUp")}>
                Sign Up
              </Button>
            </View>
            
          <View>
            <TouchableOpacity onPress={() => navigation.navigate("ResetPassword")}>
              <Text grayColor style={{ marginTop: 25 }}>
                Forgot your password?
              </Text>
            </TouchableOpacity>
          </View>

          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}



const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(AuthActions, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignIn);
