import React, { Component } from "react";
import { connect } from "react-redux";
import { AuthActions } from "@actions";
import { ActivityIndicator, View, StyleSheet } from "react-native";
import { bindActionCreators } from "redux";
import { Images, BaseColor, BaseSetting } from "@config";
import SplashScreen from "react-native-splash-screen";
import { Image, Text } from "@components";
import styles from "./styles";

class Loading extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        SplashScreen.hide();
        setTimeout(() => {
            this.props.navigation.navigate("Main");
        }, 500);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ alignItems: "center" }}>
                    <Image
                        // source={Images.logo}
                        source={require('../../components/Image/logo.jpg')}
                        style={styles.logo}
                        resizeMode="contain"
                        // style={styles_.ImageStyle}
                    />
                    <Text title1 style={{ marginTop: 10 }}>
                    
                    </Text>
                    <Text headline primaryColor style={{ marginTop: 10 }}>
                        Grace of Christ Church Community (GCCC)
                    </Text>
                </View>
                <ActivityIndicator
                    size="large"
                    color={BaseColor.textPrimaryColor}
                    style={{
                        position: "absolute",
                        top: 260,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                />
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        auth: state.auth
    };
};

const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators(AuthActions, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Loading);


const styles_ = StyleSheet.create({
    ImageStyle: { padding: 10, margin: 5, height: 25, width: 25, resizeMode: 'stretch', alignItems: 'center' }
  });
