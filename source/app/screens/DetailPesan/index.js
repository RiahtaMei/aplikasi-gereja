import React, { Component } from "react";
import {
    View,
    ScrollView,
    FlatList,
    Animated,
    TouchableOpacity,
    Linking,
    Alert
} from "react-native";
import { BaseStyle, BaseColor, Images } from "@config";
import {
    Header,
    SafeAreaView,
    Icon,
    Text,
    StarRating,
    Tag,
    Image,
    PlaceItem,
    CardList,
    Button
} from "@components";
import MapView, { PROVIDER_GOOGLE, Marker } from "react-native-maps";
import * as Utils from "@utils";
import styles from "./styles";

// Load sample data
import { PlaceListData, ReviewData } from "@data";

export default class DetailPesan extends Component {
    constructor(props) {
        super(props);

        // Temp data define
        this.state = {
            collapseHour: true,
            index: 0,
            routes: [
                { key: "information", title: "Information" },
                { key: "review", title: "Review" },
                { key: "feedback", title: "Feedback" },
                { key: "map", title: "Map" }
            ],
            heightHeader: Utils.heightHeader(),
            information: [
                {
                    id: "1",
                    icon: "map-marker-alt",
                    title: "Address",
                    type: "map",
                    information: "667 Wiegand Gardens Suite, United States"
                },
                {
                    id: "2",
                    icon: "mobile-alt",
                    title: "Tel",
                    type: "phone",
                    information: "+903 9802-7892"
                },
                {
                    id: "3",
                    icon: "envelope",
                    title: "Email",
                    type: "email",
                    information: "liststar@passionui.com"
                },
                {
                    id: "4",
                    icon: "globe",
                    title: "Website",
                    type: "web",
                    information: "http://passionui.com"
                }
            ],
            workHours: [
                { id: "1", date: "Monday", hour: "09:0 AM - 18:00 PM" },
                { id: "2", date: "Tuesday", hour: "09:0 AM - 18:00 PM" },
                { id: "3", date: "Wednesday", hour: "09:0 AM - 18:00 PM" },
                { id: "4", date: "Thursday", hour: "09:0 AM - 18:00 PM" },
                { id: "5", date: "Friday", hour: "09:0 AM - 18:00 PM" },
                { id: "6", date: "Saturday", hour: "Close" },
                { id: "7", date: "Sunday", hour: "Close" }
            ],
            list: PlaceListData,
            relate: PlaceListData.slice(5, 7),
            facilities: [
                { id: "1", icon: "wifi", name: "Free Wifi", checked: true },
                { id: "2", icon: "bath", name: "Shower" },
                { id: "3", icon: "paw", name: "Pet Allowed" },
                { id: "4", icon: "bus", name: "Shuttle Bus" },
                { id: "5", icon: "cart-plus", name: "Supper Market" },
                { id: "6", icon: "clock", name: "Open 24/7" }
            ],
            region: {
                latitude: 1.352083,
                longitude: 103.819839,
                latitudeDelta: 0.009,
                longitudeDelta: 0.004
            }
        };
        this._deltaY = new Animated.Value(0);
    }

    onOpen(item) {
        Alert.alert(
            "Listar",
            "Do you want to open " + item.title + " ?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "OK",
                    onPress: () => {
                        switch (item.type) {
                            case "web":
                                Linking.openURL(item.information);
                                break;
                            case "phone":
                                Linking.openURL("tel://" + item.information);
                                break;
                            case "email":
                                Linking.openURL("mailto:" + item.information);
                                break;
                            case "map":
                                Linking.openURL(
                                    "http://maps.apple.com/?ll=37.484847,-122.148386"
                                );
                                break;
                        }
                    }
                }
            ],
            { cancelable: true }
        );
    }

    onCollapse() {
        Utils.enableExperimental();
        this.setState({
            collapseHour: !this.state.collapseHour
        });
    }

    render() {
        const { navigation } = this.props;
        const {
            heightHeader,
            information,
            workHours,
            collapseHour,
            list,
            relate,
            facilities,
            region
        } = this.state;
        const heightImageBanner = Utils.scaleWithPixel(250, 1);
        return (
            <View style={{ flex: 1 }}>
                <Animated.View
                    style={[
                        styles.imgBanner,
                        {
                            height: this._deltaY.interpolate({
                                inputRange: [
                                    0,
                                    Utils.scaleWithPixel(140),
                                    Utils.scaleWithPixel(140)
                                ],
                                outputRange: [
                                    heightImageBanner,
                                    heightHeader,
                                    heightHeader
                                ]
                            })
                        }
                    ]}
                >
                    <Image source={Images.place1} style={{ flex: 1 }} />
                    <Animated.View
                        style={{
                            position: "absolute",
                            bottom: 15,
                            left: 20,
                            flexDirection: "row",
                            opacity: this._deltaY.interpolate({
                                inputRange: [
                                    0,
                                    Utils.scaleWithPixel(140),
                                    Utils.scaleWithPixel(140)
                                ],
                                outputRange: [1, 0, 0]
                            })
                        }}
                    >
                    </Animated.View>
                </Animated.View>
                <SafeAreaView
                    style={BaseStyle.safeAreaView}
                    forceInset={{ top: "always" }}
                >
                    {/* Header */}
                    <Header
                        title=""
                        renderLeft={() => {
                            return (
                                <Icon
                                    name="arrow-left"
                                    size={20}
                                    color={BaseColor.whiteColor}
                                />
                            );
                        }}
                        onPressLeft={() => {
                            navigation.goBack();
                        }}
                    />
                    <ScrollView
                        onScroll={Animated.event([
                            {
                                nativeEvent: {
                                    contentOffset: { y: this._deltaY }
                                }
                            }
                        ])}
                        onContentSizeChange={() => {
                            this.setState({
                                heightHeader: Utils.heightHeader()
                            });
                        }}
                        scrollEventThrottle={8}
                    >
                        <View style={{ height: 255 - heightHeader }} />
                        <View
                            style={{
                                paddingHorizontal: 20,
                                marginBottom: 20
                            }}
                        >
                            <View style={styles.lineSpace}>
                                <Text title3 semibold
                                semibold
                                style={{
                                    textAlign: 'center' 
                                }}
                                >
                                    Pesan Singkat Dr. Josua Tumakaka
                                </Text>
                            </View>
                        </View>
                        <View style={styles.contentDescription}>
                            <View style={{paddingBottom:10}}>
                                <Text body2 style={{ lineHeight: 20 }}>
                                When thinking about the day, December 25, what comes up to your mind first? 
                                Is it the school break or your day off from work? The long break for New Year is coming? 
                                Is it the presents you’re expecting? Is it the warmth of family gathering and dinner? 
                                Or the church service? I won’t get surprised if the latter is the least you think of.
                                </Text>
                            </View>
                            <View style={{paddingBottom:10}}>
                                <Text body2 style={{ lineHeight: 20 }}>
                                It’s nice to see the happy faces when Christmas is coming. 
                                But it sometimes saddens me that the happiness doesn’t come from the understanding of the day, 
                                that some people don’t find their Christmas merry for not getting a break or presents. 
                                The day starts to lose its meaning. December 25 should not be the time you’re looking forward 
                                to a surprise or a day off or other worldly fun. It’s the moment you’d better feel blessed, 
                                as a Savior was born to save your life and get all your sins forgiven later. 
                                That is the biggest gift anyone receives.
                                </Text>
                            </View>
                            <View style={{paddingBottom:10}}>
                                <Text body2 style={{ lineHeight: 20 }}>
                                Don’t feel bad for not having a day off on Christmas; take it as a service. 
                                It’s something big and meaningful for others. You might look down on what you do, 
                                that you’re only a waiter, a clerk, a guard or else – but you should realize that, 
                                that is important. 
                                You won’t know, the family you’re serving at the restaurant was separated for years 
                                and your presence help them make the dinner they have precious. 
                                If you weren’t there, it might not go that great. 
                                Or you think you’re only doing an easy job serving customers at a shop. 
                                You won’t know, somebody comes there to buy a gift for someone and it makes the person 
                                feel special. Maybe he wanted to end his life the previous day, but then the gift makes 
                                his day and he changes his mind – we never know. Or if you can’t gather with your family 
                                on the day, because they are away or have left long ago; pray. Pray that you all will 
                                meet in the afterlife, after Christ saves you.
                                </Text>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    }


}


