import { Images } from "@config";

export const CategoryData3 = [
  {
    id: "1",
    screen: "Warta",
    image: Images.warta2,
    icon: "bullhorn",
    title: "Warta Jemaat - Minggu Epiphania III",
    subtitle: "26 Januari 2019"
  },
  {
    id: "2",
    screen: "Warta",
    image: Images.warta2,
    icon: "bullhorn",
    title: "Warta Jemaat - Minggu Epiphania II",
    subtitle: "19 Januari 2019"
  },
  {
    id: "3",
    screen: "Warta",
    image: Images.warta2,
    icon: "bullhorn",
    title: "Warta Jemaat - Minggu Epiphania I",
    subtitle: "12 Januari 2019"
  },
  // {
  //   id: "4",
  //   screen: "Place",
  //   image: Images.category4,
  //   icon: "hands",
  //   title: "Baptis dalam Kristus",
  //   subtitle: "25 Januari 2019"
  // },
  // {
  //   id: "5",
  //   screen: "Place",
  //   image: Images.location1,
  //   icon: "hands",
  //   title: "Sunday Service GCCC Artha",
  //   subtitle: "24 Januari 2019"
  // }
];
