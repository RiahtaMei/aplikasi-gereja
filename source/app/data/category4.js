import { Images } from "@config";

export const CategoryData4 = [
  {
    id: "1",
    screen: "DetailPesan",
    image: Images.category1,
    icon: "bullhorn",
    title: "Pesan Singkat Dr. Josua Tumakaka",
    subtitle: "26 Januari 2019"
  },
  {
    id: "2",
    screen: "DetailPesan",
    image: Images.category2,
    icon: "bullhorn",
    title: "Christmas Fellowship",
    subtitle: "19 Januari 2019"
  },
  // {
  //   id: "3",
  //   screen: "Place",
  //   image: Images.category3,
  //   icon: "bullhorn",
  //   title: "Warta Jemaat - Minggu Epiphania I",
  //   subtitle: "12 Januari 2019"
  // },
  // {
  //   id: "4",
  //   screen: "Place",
  //   image: Images.category4,
  //   icon: "hands",
  //   title: "Baptis dalam Kristus",
  //   subtitle: "25 Januari 2019"
  // },
  // {
  //   id: "5",
  //   screen: "Place",
  //   image: Images.location1,
  //   icon: "hands",
  //   title: "Sunday Service GCCC Artha",
  //   subtitle: "24 Januari 2019"
  // }
];
