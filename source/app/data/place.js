import { Images } from "@config";

const PlaceListData = [
  {
    id: "0",
    image: Images.tentangkami2,
    title: "Sejarah GCCC",
    subtitle: "Sejarah GCCC",
    location: "667 Wiegand Gardens Suite 330",
    route: "Sejarah",
    region: {
      latitude: 37.763844,
      longitude: -122.414925
    },
    active: true,
    phone: "+1-678-338-7328",
    // rate: 4.5,
    // status: "Open Now",
    // rateStatus: "Very Good",
    // numReviews: 99
  },
  {
    id: "1",
    image: Images.tentangkami2,
    title: "Visi Misi",
    subtitle: "Visi Misi",
    route: "VisiMisi",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.761844,
      longitude: -122.414625
    },
    phone: "+1-678-338-7328",
    // rate: 4.5,
    // status: "Open Now",
    // rateStatus: "Very Good",
    // numReviews: 99
  },
  {
    id: "2",
    image: Images.tentangkami2,
    title: "Pastor & Tim Pelayanan",
    subtitle: "Pastor & Tim Pelayanan",
    route: "PastordanTim",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.763844,
      longitude: -122.415625
    },
    phone: "+1-678-338-7328",
    // rate: 4.5,
    // status: "Open Now",
    // rateStatus: "Very Good",
    // numReviews: 99
  },
  {
    id: "3",
    image: Images.tentangkami2,
    title: "Struktur Organisasi",
    subtitle: "Struktur Organisasi",
    route: "StrukturOrganisasi",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.764844,
      longitude: -122.417625
    },
    phone: "+1-678-338-7328",
    // rate: 4.5,
    // status: "Open Now",
    // rateStatus: "Very Good",
    // numReviews: 99
  },
  {
    id: "4",
    image: Images.tentangkami2,
    title: "Logo GCCC",
    subtitle: "Logo GCCC",
    route: "Logo",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.765844,
      longitude: -122.418625
    },
    phone: "+1-678-338-7328",
    // rate: 4.5,
    // status: "Open Now",
    // rateStatus: "Very Good",
    // numReviews: 99
  },
  {
    id: "5",
    image: Images.glc2,
    title: "GLC Artha Tj. Priok",
    subtitle: "Ibadah GLC untuk wijk Jakarta Utara" + 
    "\nTema ibadah kali ini adalah Yesus dan Budaya" +
    "\nDengan Pembicara Mr. Teng Hu dari Cina", 
    route: "GLC3",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Januari 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },
  {
    id: "6",
    image: Images.glc2,
    title: "GLC Artha Bekasi",
    subtitle: "Ibadah GLC untuk wijk Jakarta Timur" + 
    "\nTema ibadah kali ini adalah Warga Kerjaan Surga" +
    "\nDengan Pembicara Mr. Ricky Tampubolon dari Amerika", 
    route: "GLC4",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Januari 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },

  {
    id: "7",
    image: Images.glc2,
    title: "GLC Artha Gading",
    subtitle: "Ibadah GLC untuk wijk Jakarta Barat" + 
    "\nTema ibadah kali ini adalah Yesus dan Identitasku" +
    "\nDengan Pembicara Mr. Teng Hu dari Cina", 
    route: "GLC1",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Mei 2020',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },

  {
    id: "8",
    image: Images.glc2,
    title: "GLC Artha Mampang",
    subtitle: "Ibadah GLC untuk wijk Jakarta Selatan" + 
    "\nTema ibadah kali ini adalah Yesus dan Indonesia" +
    "\nDengan Pembicara Mr. Ricky Tampubolon dari Amerika", 
    route: "GLC2",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Mei 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },

  {
    id: "9",
    image: Images.pelayanan2,
    title: "Baptisan dalam Kristus",
    subtitle: "Baptisan dalam Kristus", 
    route: "DetailLayananGereja",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Januari 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },

  {
    id: "10",
    image: Images.pelayanan2,
    title: "Pemberkatan Nikah",
    subtitle: "Pemberkatan Nikah", 
    route: "DetailLayananGereja2",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Januari 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },

  {
    id: "11",
    image: Images.pelayanan2,
    title: "Pelayanan Doa Kesembuhan",
    subtitle: "Pelayanan Doa Kesembuhan", 
    route: "DetailLayananGereja3",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Januari 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },
    {
    id: "12",
    image: Images.video2,
    title: "Menghadirkan Allah dalam Kehidupan sehari-hari",
    subtitle: "Minggu Epiphania I", 
    route: "Video1",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '12 Januari 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },

  {
    id: "13",
    image: Images.glc2,
    title: "GLC Artha Tj. Priok",
    subtitle: "Ibadah GLC untuk wijk Jakarta Utara" + 
    "\nTema ibadah kali ini adalah Yesus dan Budaya" +
    "\nDengan Pembicara Mr. Teng Hu dari Cina", 
    route: "GLC3",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Januari 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },
  {
    id: "14",
    image: Images.glc2,
    title: "GLC Artha Bekasi",
    subtitle: "Ibadah GLC untuk wijk Jakarta Timur" + 
    "\nTema ibadah kali ini adalah Warga Kerjaan Surga" +
    "\nDengan Pembicara Mr. Ricky Tampubolon dari Amerika", 
    route: "GLC4",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Januari 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },

  {
    id: "15",
    image: Images.glc2,
    title: "GLC Artha Gading",
    subtitle: "Ibadah GLC untuk wijk Jakarta Barat" + 
    "\nTema ibadah kali ini adalah Yesus dan Identitasku" +
    "\nDengan Pembicara Mr. Teng Hu dari Cina", 
    route: "GLC1",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Mei 2020',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },

  {
    id: "16",
    image: Images.glc2,
    title: "GLC Artha Mampang",
    subtitle: "Ibadah GLC untuk wijk Jakarta Selatan" + 
    "\nTema ibadah kali ini adalah Yesus dan Indonesia" +
    "\nDengan Pembicara Mr. Ricky Tampubolon dari Amerika", 
    route: "GLC2",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Mei 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },
  {
    id: "17",
    image: Images.glc2,
    title: "GLC Artha Rawamangun",
    subtitle: "Ibadah GLC untuk wijk Jakarta Barat" + 
    "\nTema ibadah kali ini adalah Yesus dan Identitasku" +
    "\nDengan Pembicara Mr. Teng Hu dari Cina", 
    route: "GLC5",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Mei 2020',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },

  {
    id: "18",
    image: Images.glc2,
    title: "GLC Artha Duren Tiga",
    subtitle: "Ibadah GLC untuk wijk Jakarta Selatan" + 
    "\nTema ibadah kali ini adalah Yesus dan Indonesia" +
    "\nDengan Pembicara Mr. Ricky Tampubolon dari Amerika", 
    route: "GLC6",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Mei 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },

  {
    id: "19",
    image: Images.glc2,
    title: "GLC Artha Grogol",
    subtitle: "Ibadah GLC untuk wijk Jakarta Selatan" + 
    "\nTema ibadah kali ini adalah Yesus dan Indonesia" +
    "\nDengan Pembicara Mr. Ricky Tampubolon dari Amerika", 
    route: "GLC7",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Mei 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },

  {
    id: "20",
    image: Images.glc2,
    title: "GLC Artha Pejaten Village",
    subtitle: "Ibadah GLC untuk wijk Jakarta Selatan" + 
    "\nTema ibadah kali ini adalah Yesus dan Indonesia" +
    "\nDengan Pembicara Mr. Ricky Tampubolon dari Amerika", 
    route: "GLC8",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Mei 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },



];

export { PlaceListData };
