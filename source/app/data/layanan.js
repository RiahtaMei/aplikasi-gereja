import { Images } from "@config";

const PlaceListData = [
  {
    id: "0",
    image: Images.trip2,
    title: "Sejarah GCCC",
    subtitle: "Sejarah GCCC",
    location: "667 Wiegand Gardens Suite 330",
    route: "Sejarah",
    region: {
      latitude: 37.763844,
      longitude: -122.414925
    },
    active: true,
    phone: "+1-678-338-7328",
    // rate: 4.5,
    // status: "Open Now",
    // rateStatus: "Very Good",
    // numReviews: 99
  },
  {
    id: "1",
    image: Images.visimisi,
    title: "Visi Misi",
    subtitle: "Visi Misi",
    route: "VisiMisi",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.761844,
      longitude: -122.414625
    },
    phone: "+1-678-338-7328",
    // rate: 4.5,
    // status: "Open Now",
    // rateStatus: "Very Good",
    // numReviews: 99
  },
  {
    id: "2",
    image: Images.mimbar,
    title: "Pastor & Tim Pelayanan",
    subtitle: "Pastor & Tim Pelayanan",
    route: "PastordanTim",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.763844,
      longitude: -122.415625
    },
    phone: "+1-678-338-7328",
    // rate: 4.5,
    // status: "Open Now",
    // rateStatus: "Very Good",
    // numReviews: 99
  },
  {
    id: "3",
    image: Images.room6,
    title: "Struktur Organisasi",
    subtitle: "Struktur Organisasi",
    route: "StrukturOrganisasi",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.764844,
      longitude: -122.417625
    },
    phone: "+1-678-338-7328",
    // rate: 4.5,
    // status: "Open Now",
    // rateStatus: "Very Good",
    // numReviews: 99
  },
  {
    id: "4",
    image: Images.room5,
    title: "Logo GCCC",
    subtitle: "Logo GCCC",
    route: "Logo",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.765844,
      longitude: -122.418625
    },
    phone: "+1-678-338-7328",
    // rate: 4.5,
    // status: "Open Now",
    // rateStatus: "Very Good",
    // numReviews: 99
  },
  {
    id: "5",
    image: Images.trip3,
    title: "GLC Artha Tj. Priok",
    subtitle: "Ibadah GLC untuk wijk Jakarta Utara" + 
    "\nTema ibadah kali ini adalah Yesus dan Budaya" +
    "\nDengan Pembicara Mr. Teng Hu dari Cina", 
    route: "GLC3",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Januari 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },
  {
    id: "6",
    image: Images.trip8,
    title: "GLC Artha Bekasi",
    subtitle: "Ibadah GLC untuk wijk Jakarta Timur" + 
    "\nTema ibadah kali ini adalah Warga Kerjaan Surga" +
    "\nDengan Pembicara Mr. Ricky Tampubolon dari Amerika", 
    route: "GLC4",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Mei 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },

  {
    id: "7",
    image: Images.place2,
    title: "GLC Artha Gading",
    subtitle: "Ibadah GLC untuk wijk Jakarta Barat" + 
    "\nTema ibadah kali ini adalah Yesus dan Identitasku" +
    "\nDengan Pembicara Mr. Teng Hu dari Cina", 
    route: "GLC1",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Januari 2020',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },

  {
    id: "8",
    image: Images.place1,
    title: "GLC Artha Mampang",
    subtitle: "Ibadah GLC untuk wijk Jakarta Selatan" + 
    "\nTema ibadah kali ini adalah Yesus dan Indonesia" +
    "\nDengan Pembicara Mr. Ricky Tampubolon dari Amerika", 
    route: "GLC2",
    location: "667 Wiegand Gardens Suite 330",
    region: {
      latitude: 37.766644,
      longitude: -122.419125
    },
    phone: "+1-678-338-7328",
    rate: '26 Januari 2019',
    status: "Open Now",
    rateStatus: "Very Good",
    numReviews: 99
  },



];

export { PlaceListData };
