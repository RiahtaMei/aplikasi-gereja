import { LanguageData } from "./language";
import { UserData } from "./user";
import { PlaceListData } from "./place";
import { Layanan } from "./layanan";
import { MessagesData } from "./messages";
import { NotificationData } from "./notification";
import { ReviewData } from "./review";
import { CategoryData } from "././category";
import { CategoryData2 } from "././category2";
import { CategoryData3 } from "././category3";
import { CategoryData4 } from "././category4";
import { AboutUsData } from "./aboutUs";
import { WhislistData } from "./whislist";
import { WhislistData2 } from "./whislist2";
import { HomeBannerData } from "./homebanner";
import { HomeListData } from "./homelist";
import { HomePopularData } from "./homepopulars";
import { HomeServicesData } from "./homeservices";
// Sample data for display on template
export {
  HomeServicesData,
  HomePopularData,
  HomeListData,
  HomeBannerData,
  WhislistData,
  WhislistData2,
  AboutUsData,
  CategoryData,
  CategoryData2,
  CategoryData3,
  CategoryData4,
  LanguageData,
  UserData,
  PlaceListData,
  Layanan,
  MessagesData,
  NotificationData,
  ReviewData
};
