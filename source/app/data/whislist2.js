import { Images } from "@config";
export const WhislistData2 = [
  {
    id: "1",
    image: Images.pujian1,
    title: "Kunyanyikan Kasih Setia Tuhan",
    subtitle: "Tema: Pujian Penyembahan",
    route: "Lirik"
    // rate: 4.5
  },
  {
    id: "2",
    image: Images.pujian1,
    title: "Janji yang Manis, kau tak kulupakan",
    subtitle: "Tema: Pujian Penyembahan",
    route: "Lirik2"
    // rate: 4.5
  },
  {
    id: "3",
    image: Images.pujian1,
    title: "Kusiapkan Hatiku",
    subtitle: "Tema: Pujian memasuki Khotbah",
    route: "Lirik3"
    // rate: 4.5
  },
  {
    id: "4",
    image: Images.pujian1,
    title: "Hendaklah Hidupmu Sesuai Injil",
    subtitle: "Tema: Pujian Penyembahan",
    route: "Lirik4"
    // rate: 4.5
  },
  {
    id: "5",
    image: Images.pujian1,
    title: "Hidup Bagi Kristus",
    subtitle: "Tema: Pujian Penyembahan",
    route: "Lirik5"
    // rate: 4.5
  },
  {
    id: "6",
    image: Images.pujian1,
    title: "Ku Sungguh Berharga",
    subtitle: "Tema: Pujian Penyembahan",
    route: "Lirik6"
    // rate: 4.5
  }
];
