import { Images } from "@config";
export const WhislistData = [
  {
    id: "1",
    image: Images.trip2,
    title: "New Hall GCCC ",
    subtitle: "Mall Artha Gading",
    rate: 4.5,
    route: "DetailLokasi"
  },
  {
    id: "2",
    image: Images.trip2,
    title: "Ruko Klampis",
    subtitle: "Square Blok B-1",
    rate: 4.5,
    route: "DetailLokasi"
  },
  {
    id: "3",
    image: Images.trip2,
    title: "Aston Semarang Hotel",
    // subtitle: "Arts & Humanities",
    rate: 4.5,
    route: "DetailLokasi"
  },
  // {
  //   id: "4",
  //   image: Images.location4,
  //   title: "Hilton San Francisco",
  //   subtitle: "Arts & Humanities",
  //   rate: 4.5
  // },
  // {
  //   id: "5",
  //   image: Images.location7,
  //   title: "Lounge Coffee Bar",
  //   subtitle: "Arts & Humanities",
  //   rate: 4.5
  // },
  // {
  //   id: "6",
  //   image: Images.service4,
  //   title: "Taylor Swift & Friends",
  //   subtitle: "Events & Shows",
  //   rate: 4.5
  // }
];
