import { Images } from "@config";

export const HomeBannerData = [
  { id: "1", image: Images.gereja3 },
  { id: "2", image: Images.gereja2 },
  { id: "3", image: Images.gereja1 }
];
