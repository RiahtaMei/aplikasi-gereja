import { Images } from "@config";

export const CategoryData = [
  {
    id: "1",
    screen: "DetailRenunganHarian",
    image: Images.renungan2,
    icon: "bible",
    title: "Bejana Tanah Liat Antik",
    subtitle: "28 Januari 2019"
  },
  {
    id: "2",
    screen: "DetailRenunganHarian2",
    image: Images.renungan2,
    icon: "bible",
    title: "Bangku Persahabatan",
    subtitle: "27 Januari 2019"
  },
  {
    id: "3",
    screen: "DetailRenunganHarian3",
    image: Images.renungan2,
    icon: "bible",
    title: "Membawa Anak kepada Allah",
    subtitle: "26 Januari 2019"
  },
  {
    id: "4",
    screen: "DetailRenunganHarian4",
    image: Images.renungan2,
    icon: "bible",
    title: "Misteri Terbesar",
    subtitle: "25 Januari 2019"
  },
  {
    id: "5",
    screen: "DetailRenunganHarian5",
    image: Images.renungan2,
    icon: "bible",
    title: "Tidak Perlu Antre",
    subtitle: "24 Januari 2019"
  }
];
