import {
  BaseColor,
  BlueColor,
  PinkColor,
  GreenColor,
  YellowColor
} from "@config";

export const HomeServicesData = [
  {
    id: "1",
    color: BaseColor.lightPrimaryColor,
    icon: "church",
    name: "Tentang Kami",
    route: "TentangKami"
  },
  {
    id: "2",
    color: BaseColor.kashmir,
    icon: "hands",
    name: "Pelayanan",
    route: "Pelayanan"
  },
  {
    id: "3",
    color: PinkColor.primaryColor,
    icon: "handshake",
    name: "Layanan Gereja",
    route: "LayananGereja"
  },
  {
    id: "4",
    color: BlueColor.primaryColor,
    icon: "bullhorn",
    name: "Warta",
    route: "Warta"
  },
  {
    id: "5",
    color: BaseColor.accentColor,
    icon: "bible",
    name: "Renungan",
    route: "Renungan"
  },
  {
    id: "3",
    color: GreenColor.primaryColor,
    icon: "users",
    name: "Grace Life Cell",
    route: "GLC"
  },
  {
    id: "4",
    color: YellowColor.primaryColor,
    icon: "music",
    name: "Pujian",
    route: "Pujian"
  },
  {
    id: "5",
    color: BaseColor.kashmir,
    icon: "youtube",
    name: "Video",
    route: "Video"
  }
];
