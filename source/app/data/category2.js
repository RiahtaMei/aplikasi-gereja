import { Images } from "@config";

export const CategoryData2 = [
  {
    id: "1",
    screen: "DetailPelayanan",
    image: Images.jadwal2,
    icon: "hands",
    title: "Kebaktian Pemuda",
    subtitle: "28 Januari 2019"
  },
  {
    id: "2",
    screen: "DetailPelayanan2",
    image: Images.jadwal2,
    icon: "hands",
    title: "GLC Manado",
    subtitle: "27 Januari 2019"
  },
  {
    id: "3",
    screen: "DetailPelayanan3",
    image: Images.jadwal2,
    icon: "hands",
    title: "GLC Sorong",
    subtitle: "26 Januari 2019"
  },
  {
    id: "4",
    screen: "DetailPelayanan4",
    image: Images.jadwal2,
    icon: "hands",
    title: "Baptis dalam Kristus",
    subtitle: "25 Januari 2019"
  },
  {
    id: "5",
    screen: "DetailPelayanan5",
    image: Images.jadwal2,
    icon: "hands",
    title: "Sunday Service GCCC Artha",
    subtitle: "24 Januari 2019"
  }
];
