import React from "react";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { createStackNavigator } from "react-navigation-stack";
import { BaseColor, BaseStyle } from "@config";
import { Icon } from "@components";
import * as Utils from "@utils";
import { store } from "app/store";

/* Bottom Screen */
import Home from "@screens/Home";
import Category from "@screens/Category";
import Place from "@screens/Place";
import Whislist from "@screens/Whislist";
import Profile from "@screens/Profile";

/* Modal Screen only affect iOS */
import Filter from "@screens/Filter";
import Search from "@screens/Search";
import SearchHistory from "@screens/SearchHistory";
import PreviewImage from "@screens/PreviewImage";
import SignUp from "@screens/SignUp";
import SignIn from "@screens/SignIn";

/* Stack Screen */
import Messenger from "@screens/Messenger";
import Review from "@screens/Review";
import Feedback from "@screens/Feedback";
import Messages from "@screens/Messages";
import Notification from "@screens/Notification";
import Walkthrough from "@screens/Walkthrough";

import ResetPassword from "@screens/ResetPassword";
import KonfirOTP from "@screens/KonfirOTP";
import KonfirOTP_ from "@screens/KonfirOTP_";

import ChangePassword from "@screens/ChangePassword";
import ProfileEdit from "@screens/ProfileEdit";
import ChangeLanguage from "@screens/ChangeLanguage";
import PlaceDetail from "@screens/PlaceDetail";
import ContactUs from "@screens/ContactUs";
import AboutUs from "@screens/AboutUs";

import TentangKami from "@screens/TentangKami";
import GLC from "@screens/GLC";
import GLC1 from "@screens/GLC1";
import GLC2 from "@screens/GLC2";
import GLC3 from "@screens/GLC3";
import GLC4 from "@screens/GLC4";
import GLC5 from "@screens/GLC5";
import GLC6 from "@screens/GLC6";
import GLC7 from "@screens/GLC7";
import GLC8 from "@screens/GLC8";

import KonfirKedatangan from "@screens/KonfirKedatangan";
import SuksesMendaftar from "@screens/SuksesMendaftar";

import Sejarah from "@screens/Sejarah";
import VisiMisi from "@screens/VisiMisi";
import PastordanTim from "@screens/PastordanTim";
import StrukturOrganisasi from "@screens/StrukturOrganisasi";
import Logo from "@screens/Logo";

import Renungan from "@screens/Renungan";
import DetailRenunganHarian from "@screens/DetailRenunganHarian";
import DetailRenunganHarian2 from "@screens/DetailRenunganHarian2";
import DetailRenunganHarian3 from "@screens/DetailRenunganHarian3";
import DetailRenunganHarian4 from "@screens/DetailRenunganHarian4";
import DetailRenunganHarian5 from "@screens/DetailRenunganHarian5";


import Pelayanan from "@screens/Pelayanan";
import DetailPelayanan from "@screens/DetailPelayanan";
import DetailPelayanan2 from "@screens/DetailPelayanan2";
import DetailPelayanan3 from "@screens/DetailPelayanan3";
import DetailPelayanan4 from "@screens/DetailPelayanan4";
import DetailPelayanan5 from "@screens/DetailPelayanan5";

import LayananGereja from "@screens/LayananGereja";
import DetailLayananGereja from "@screens/DetailLayananGereja";
import DetailLayananGereja2 from "@screens/DetailLayananGereja2";
import DetailLayananGereja3 from "@screens/DetailLayananGereja3";

import Warta from "@screens/Warta";
import Pujian from "@screens/Pujian";
import Video from "@screens/Video";
import Video1 from "@screens/Video1";
import Lirik from "@screens/Lirik";
import Lirik2 from "@screens/Lirik2";
import Lirik3 from "@screens/Lirik3";
import Lirik4 from "@screens/Lirik4";
import Lirik5 from "@screens/Lirik5";
import Lirik6 from "@screens/Lirik6";

import Pesan from "@screens/Pesan";
import DetailPesan from "@screens/DetailPesan";

import Lokasi from "@screens/Lokasi";
import DetailLokasi from "@screens/DetailLokasi";

import ProfileEdit2 from "@screens/ProfileEdit2";


// Transition for navigation by screen name
const handleCustomTransition = ({ scenes }) => {
    const nextScene = scenes[scenes.length - 1].route.routeName;
    switch (nextScene) {
        case "PreviewImage":
            Utils.enableExperimental();
            return Utils.zoomIn();
        default:
            return false;
    }
};

// Config for bottom navigator
const bottomTabNavigatorConfig = {
    initialRouteName: "Home",
    tabBarOptions: {
        showIcon: true,
        showLabel: true,
        activeTintColor: BaseColor.primaryColor,
        inactiveTintColor: BaseColor.grayColor,
        style: BaseStyle.tabBar,
        labelStyle: {
            fontSize: 12
        }
    }
};

// Tab bar navigation
const routeConfigs = {
    Home: {
        screen: Home,
        navigationOptions: ({ navigation }) => ({
            title: "Home",
            tabBarIcon: ({ focused, tintColor }) => {
                return (
                    <Icon color={tintColor} name="home" size={20} solid />
                );
            }
        })
    },
    Category: {
        screen: Pesan,
        navigationOptions: ({ navigation }) => ({
            title: "Pesan",
            tabBarIcon: ({ focused, tintColor }) => {
                return <Icon color={tintColor} name="inbox" size={20} solid />;
            }
        })
    },
    Place: {
        screen: Lokasi,
        navigationOptions: ({ navigation }) => ({
            title: "Lokasi",
            tabBarIcon: ({ focused, tintColor }) => {
                return (
                    <Icon
                        solid
                        color={tintColor}
                        name="map-marker-alt"
                        size={20}
                        solid
                    />
                );
            }
        })
    },
    Whislist: {
        // screen: Whislist,
        screen: ProfileEdit2,
        navigationOptions: ({ navigation }) => ({
            title: "Keanggotaan",
            tabBarIcon: ({ focused, tintColor }) => {
                return (
                    <Icon
                        solid
                        color={tintColor}
                        name="user-circle"
                        size={20}
                        solid
                    />
                );
            }
        })
    },
    Profile: {
        screen: Profile,
        navigationOptions: ({ navigation }) => ({
            title: "Pengaturan",
            tabBarIcon: ({ focused, tintColor }) => {
                return (
                    <Icon
                        solid
                        color={tintColor}
                        name="whmcs"
                        size={20}
                    />
                );
            }
        })
    }
};

// Define bottom navigator as a screen in stack
const BottomTabNavigator = createBottomTabNavigator(
    routeConfigs,
    bottomTabNavigatorConfig
);

// Main Stack View App
const StackNavigator = createStackNavigator(
    {
        BottomTabNavigator: {
            screen: BottomTabNavigator
        },
        Walkthrough: {
            screen: Walkthrough
        },
        Profile: {
            screen: Profile
        },
        SignUp: {
            screen: SignUp
        },
        SignIn: {
            screen: SignIn
        },
        Review: {
            screen: Review
        },
        Feedback: {
            screen: Feedback
        },
        Messages: {
            screen: Messages
        },
        Notification: {
            screen: Notification
        },
        KonfirOTP: {
            screen: KonfirOTP
        },
        KonfirOTP_: {
            screen: KonfirOTP_
        },
        ResetPassword: {
            screen: ResetPassword
        },
        ChangePassword: {
            screen: ChangePassword
        },
        ProfileEdit: {
            screen: ProfileEdit
        },
        ChangeLanguage: {
            screen: ChangeLanguage
        },
        Messenger: {
            screen: Messenger
        },
        PlaceDetail: {
            screen: PlaceDetail
        },
        TentangKami: {
            screen: TentangKami
        },
        GLC: {
            screen: GLC
        },
        GLC1: {
            screen: GLC1
        },
        GLC2: {
            screen: GLC2
        },
        GLC3: {
            screen: GLC3
        },
        GLC4: {
            screen: GLC4
        },
        GLC5: {
            screen: GLC5
        },
        GLC6: {
            screen: GLC6
        },
        GLC7: {
            screen: GLC7
        },
        GLC8: {
            screen: GLC8
        },
        KonfirKedatangan: {
            screen: KonfirKedatangan
        },
        SuksesMendaftar: {
            screen: SuksesMendaftar
        },
        Sejarah: {
            screen: Sejarah
        },
        VisiMisi: {
            screen: VisiMisi
        },
        PastordanTim: {
            screen: PastordanTim
        },
        StrukturOrganisasi: {
            screen: StrukturOrganisasi
        },
        Logo: {
            screen: Logo
        },
        ContactUs: {
            screen: ContactUs
        },
        AboutUs: {
            screen: AboutUs
        },
        Renungan: {
            screen: Renungan
        },
        DetailRenunganHarian: {
            screen: DetailRenunganHarian
        },
        DetailRenunganHarian2: {
            screen: DetailRenunganHarian2
        },
        DetailRenunganHarian3: {
            screen: DetailRenunganHarian3
        },
        DetailRenunganHarian4: {
            screen: DetailRenunganHarian4
        },
        DetailRenunganHarian5: {
            screen: DetailRenunganHarian5
        },
        Pelayanan: {
            screen: Pelayanan
        },
        DetailPelayanan: {
            screen: DetailPelayanan
        },
        DetailPelayanan2: {
            screen: DetailPelayanan2
        },
        DetailPelayanan3: {
            screen: DetailPelayanan3
        },
        DetailPelayanan4: {
            screen: DetailPelayanan4
        },
        DetailPelayanan5: {
            screen: DetailPelayanan5
        },
        LayananGereja: {
            screen: LayananGereja
        },
        DetailLayananGereja: {
            screen: DetailLayananGereja
        },
        DetailLayananGereja2: {
            screen: DetailLayananGereja2
        },
        DetailLayananGereja3: {
            screen: DetailLayananGereja3
        },
        Warta: {
            screen: Warta
        },
        Pujian: {
            screen: Pujian
        },
        Lirik: {
            screen: Lirik
        },
        Lirik2: {
            screen: Lirik2
        },
        Lirik3: {
            screen: Lirik3
        },
        Lirik4: {
            screen: Lirik4
        },
        Lirik5: {
            screen: Lirik5
        },
        Lirik6: {
            screen: Lirik6
        },
        Video: {
            screen: Video
        },
        Video1: {
            screen: Video1
        },
        DetailLokasi: {
            screen: DetailLokasi
        },
        DetailPesan: {
            screen: DetailPesan
        }
        
    },
    {
        headerMode: "none",
        initialRouteName: "BottomTabNavigator"
    }
);

// Define Root Stack support Modal Screen
const RootStack = createStackNavigator(
    {
        Filter: {
            screen: Filter
        },
        Search: {
            screen: Search
        },
        SearchHistory: {
            screen: SearchHistory
        },
        PreviewImage: {
            screen: PreviewImage
        },
        StackNavigator: {
            screen: StackNavigator
        }
    },
    {
        mode: "modal",
        headerMode: "none",
        initialRouteName: "StackNavigator",
        transitionConfig: screen => {
            return handleCustomTransition(screen);
        },
        transparentCard: true
    }
);

/* Permistion check authenticate*/
const defaultGetStateForAction = StackNavigator.router.getStateForAction;
const screenPermission = ["Profile"];

StackNavigator.router.getStateForAction = (action, state) => {
    const login = store.getState().auth.login.success;
    if (state && screenPermission.indexOf(action.routeName) > -1 && !login) {
        const routes = [
            ...state.routes,
            { key: "signin", routeName: "SignIn" }
        ];
        return {
            ...state,
            routes,
            index: routes.length - 1
        };
    }
    return defaultGetStateForAction(action, state);
};

export default RootStack;
